# !/usr/bin/python
# -*-coding: utf-8-*-
# Requiere tkinter instalado:   # dnf install python3-tkinter

########################################################################
#                               IMPORT
########################################################################
from tkinter import *
from tkinter import ttk

##############################################
#          Llegim la taula "clientes"        #
##############################################

import os

'''
CAl fer "dnf -y install python3-psycopg2"
Editem l’arxiu pg_hba.conf (nano /var/lib/pgsql/data/pg_hba.conf)
sys
Comentem la línia:
 #local   all             all                                     peer

Afegim la línea:
local   all             all                                     trust
'''
import psycopg2


##############################################
#          Ens connectem a la BBDD           #
##############################################
try:
	conn = psycopg2.connect(database="training", user="postgres", password="jupiter")
	print("DATABASE OPENED SUCCESSFULLY \n")
	
except:
	print("CONNECTION ERROR")
	exit(2)



##############################################
#            Declarem el cursor              #
##############################################
cur = conn.cursor()

os.system('clear')

##############################################
#              Menu principal                #
##############################################
# Definimos la ventana
ventana = Tk()
ventana.title("Tabla clientes")
ventana.geometry('800x400')

# Definimos los elementos de la ventana
tree = ttk.Treeview(ventana)

# Formateamos las columnas del tree
tree['show'] = 'headings'   # Para no mostrar la 1a columna del ID
tree["columns"] = ("c1", "c2", "c3", "c4")

tree.heading("c1", text="NUM_CLIE")
tree.heading("c2", text="EMPRESA")
tree.heading("c3", text="REP_CLIE")
tree.heading("c4", text="LIMITE_CREDITO")

try:
        sql="SELECT * FROM clientes"
        cur.execute(sql);
        rows = cur.fetchall()
        

        for row in rows:
            tree.insert("", 0, text="", values=(row[0], row[1], row[2], row[3]) ),

        # Mostramos los elementos de la ventana        
        tree.pack(side = 'left', expand = 1, fill = 'both') # Hacemos que el tree ocupe toda la ventana
        

        # Bucle de la ventana
        ventana.mainloop()

except psycopg2.Error as er :
    print("-------- ERROR:", er.pgcode, " -------- \n")
    conn.rollback()

##############################################
#        Ens desconnectem de la BBDD         #
##############################################
conn.close()

import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
"""
'g' is a special object that is unique for each request. It is used to store data
that might be accessed by multiple functions during the request.
"""
from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')

"""
Vista /register del Blueprint /auth -> /auth/register
"""
@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        if not username:
            error = 'Usuario obligatorio.'
        elif not password:
            error = 'Password obligatorio.'
        elif db.execute(
            'SELECT id FROM user WHERE username = ?', (username,)
        ).fetchone() is not None:
            error = 'El usuario {} ya se encuentra registrado.'.format(username)

        if error is None:
            db.execute(
                'INSERT INTO user (username, password) VALUES (?, ?)',
                (username, generate_password_hash(password))
            )
            db.commit()
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html')


"""
Vista /login del Blueprint /auth -> /auth/login
"""    
@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM user WHERE username = ?', (username,)
        ).fetchone()

        if user is None:
            error = 'Usuario incorrecto.'
        elif not check_password_hash(user['password'], password):
            error = 'Password incorrecto.'

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for('index'))

        flash(error)

    return render_template('auth/login.html')

"""
Vista /logout del Blueprint /auth -> /auth/logout
""" 
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


"""
Función del Blueprint /auth que comprueba si el usuario está loginado
Si lo está informa 'g.user'
La sesión se gestiona con una coockie firmada (por seguridad)
que contiene el 'user_id'
"""
@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()


"""
Decorador que hace un wrap de la vista (url) que lo utilice
Si el usuario no está loginado lo envia a la url de login /auth/login
"""
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view
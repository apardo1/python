# coding:utf-8
"""
Página web multipestaña:
    home
    sumar   -> Suma dos números enteros (positivos o negativos)
    about
    contact
"""

########################################################################
#                               IMPORT
########################################################################
from flask import Flask, flash, get_flashed_messages, redirect, render_template, \
     request, url_for
from datetime import datetime
import re


########################################################################
#                   VARIABLES GLOBALES Y CONSTANTES
########################################################################



########################################################################
#                               NIVEL 4
########################################################################



########################################################################
#                               NIVEL 3
########################################################################



########################################################################
#                               NIVEL 2
########################################################################
def control_errores_es_entero(variable_string):
    """
    Comprueba si el string pasado como parámetro es entero

    Returns:
        Boolean -- Si es entero->True
    """

    try:
        int(variable_string)
        es_entero = True
    except ValueError:
        es_entero = False

    return es_entero



########################################################################
#                               NIVEL 1 (MAIN)
########################################################################
app = Flask(__name__)

# "secretkey" es necesario para usar flash() para los errores
# Lo hace mdiante la sesión, que se hace con una cookie
# firmada con esta clave para evitar que manipulen su contenido
app.secret_key = 'many random bytes'

@app.route("/")
def home():
    return render_template("home.html")


@app.route("/about/")
def about():
    return render_template("about.html")

@app.route("/contact/")
def contact():
    return render_template("contact.html")


@app.route("/api/data")
def get_data():
    return app.send_static_file("data.json")


@app.route("/sumar/", methods=('GET', 'POST'))
def sumar():
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':
        resultado=0
        return render_template("sumar.html",resultado=resultado)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        # Leemos los valores introducidos en el formulario
        operando1 = request.form['operando1']
        operando2 = request.form['operando2']

        # Inicializamos el resultado
        resultado = 0

        # Inicializamos el error (por defecto no hay error)
        error = None

        # Si han dejado en blanco el campo "operando1"
        if not operando1:
            error = 'Ha ocurrido un error: el operando1 es obligatorio'
            flash(error)
        # Comprobamos si operando1 es entero
        elif control_errores_es_entero(operando1):
            operando1_int=int(operando1)
        else:
            error = 'Ha ocurrido un error: el operando1 debe ser un entero'
            flash(error)

        # Si han dejado en blanco el campo "operando2"
        if not operando2:
            error = 'Ha ocurrido un error: el operando2 es obligatorio'
            flash(error)
        # Comprobamos si operando2 es entero
        elif control_errores_es_entero(operando2):
            operando2_int=int(operando2)
        else:
            error = 'Ha ocurrido un error: el operando2 debe ser un entero'
            flash(error)

        if not get_flashed_messages():
            resultado = operando1_int + operando2_int

        return render_template("sumar.html",resultado=resultado)


if __name__ == "__main__":
    app.run(debug=True, port=5000)
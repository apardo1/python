# coding:utf-8
"""
Página web multipestaña:
    home
    sumar   -> Suma dos números enteros (positivos o negativos)
    mostrar tabla -> Muestra cualquier tabla de la BD potsgres seleccionada
    about
    contact
"""

########################################################################
#                               IMPORT
########################################################################
from flask import Flask, flash, get_flashed_messages, redirect, render_template, \
     request, url_for
# Para mostrar errores con flash y bootstrap tenemos los tipos:
#   success, info, warning, danger 

from datetime import datetime
import re
import psycopg2

# Para poder mostrar campos de la BD de tipo utf8 (ñ,ç,à,á,...)
import sys
if sys.version_info.major < 3:
    reload(sys)
sys.setdefaultencoding('utf8')


########################################################################
#                   VARIABLES GLOBALES Y CONSTANTES
########################################################################



########################################################################
#                               NIVEL 4
########################################################################



########################################################################
#                               NIVEL 3
########################################################################



########################################################################
#                               NIVEL 2
########################################################################
def control_errores_es_entero(variable_string):
    """
    Comprueba si el string pasado como parámetro es entero

    Returns:
        Boolean -- Si es entero->True
    """

    try:
        int(variable_string)
        es_entero = True
    except ValueError:
        es_entero = False

    return es_entero



########################################################################
#                               NIVEL 1 (MAIN)
########################################################################
app = Flask(__name__)

# "secretkey" es necesario para usar flash() para los errores
# Lo hace mdiante la sesión, que se hace con una cookie
# firmada con esta clave para evitar que manipulen su contenido
app.secret_key = 'many random bytes'

########################################################################
@app.route("/")
def home():
    return render_template("home.html")

########################################################################
@app.route("/about/")
def about():
    return render_template("about.html")

########################################################################
@app.route("/contact/")
def contact():
    return render_template("contact.html")

########################################################################
@app.route("/api/data")
def get_data():
    return app.send_static_file("data.json")

########################################################################
@app.route("/mostrar_tabla_bd/", methods=('GET', 'POST'))
def mostrar_tabla_bd():
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':
        return render_template("mostrar_tabla_bd.html",tot_rows=0)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        # Leemos los valores introducidos en el formulario
        nombre_tabla = request.form['nombre_tabla']

        # Inicializamos el error (por defecto no hay error)
        error = None

        # Si han dejado en blanco el campo "nombre_tabla"
        if not nombre_tabla:
            error = 'El nombre de la tabla es obligatorio'
            flash(error,'danger')
            return render_template("mostrar_tabla_bd.html",tot_rows=0)
        else:
            try:
                conn = psycopg2.connect(database="tienda", user="postgres", password="jupiter")
                print("DATABASE OPENED SUCCESSFULLY \n")

            except:
                print("CONNECTION ERROR")
                exit(2)

            cur = conn.cursor()

            try:
                sql="SELECT * FROM " + nombre_tabla
                cur.execute(sql)

                rows = cur.fetchall() # Obtenemos todas las filas
                tot_rows = rows.__len__() # Obtenemos el número total de filas
                tot_columns = rows[0].__len__() # Obtenemos el número total de columnas
                list_columns = [columnames[0] for columnames in cur.description] # Obtenemos las cabecceras

            except psycopg2.Error as er :
                # Error=42P01   ->   No existe la tabla en la BD
                if (er.pgcode=="42P01"):
                    error = 'La tabla no existe en la BD'
                    flash(error,'danger')
                    return render_template("mostrar_tabla_bd.html",tot_rows=0)
                else:
                    print("-------- ERROR:", er.pgcode, " -------- \n")
                conn.rollback()

            conn.close()

            return render_template("mostrar_tabla_bd.html",
                                tot_rows=tot_rows,rows=rows,
                                tot_columns=tot_columns,list_columns=list_columns,
                                nombre_tabla=nombre_tabla,)

########################################################################
@app.route("/modificar_empleados/<int:id>/")
def modificar_empleados(id):
    # Inicializamos el error (por defecto no hay error)
    error = None

    try:
        conn = psycopg2.connect(database="tienda", user="postgres", password="jupiter")
        print("DATABASE OPENED SUCCESSFULLY \n")

    except:
        print("CONNECTION ERROR")
        exit(2)

    cur = conn.cursor()

    try:
        sql="SELECT * FROM empleados " + \
            "WHERE id=" + str(id)
        cur.execute(sql)

        rows = cur.fetchall() # Obtenemos todas las filas
        nombre          = rows[0][1]
        numero_telefono = rows[0][2]

    except psycopg2.Error as er :
        print("DATABASE ERROR")
        exit(2)
        conn.rollback()

    conn.close()

    url="/modificar_empleados_form/" + str(id) + "/" + str(nombre) + "/" + str(numero_telefono)
    return redirect(url)

########################################################################
@app.route("/modificar_empleados_form/<int:id>/<nombre>/<numero_telefono>/", methods=('GET', 'POST'))
def modificar_empleados_form(id,nombre,numero_telefono):
    # La primera vez que se carga la página del formulario lo hace con GET
    if request.method == 'GET':
        # Inicializamos el error (por defecto no hay error)
        error = None


        return render_template("modificar_empleados_form.html",
                                id=id,
                                nombre=nombre,
                                numero_telefono=numero_telefono)

    # Cuando el usuario ya ha entrado en el formulario y lo ha enviado con
    # el botón "Submit" se hace con POST
    elif request.method == 'POST':
        # Leemos los valores introducidos en el formulario
        nombre = request.form['nombre']
        numero_telefono = request.form['numero_telefono']

        return render_template("modificar_empleados_form.html",
                                id=id,
                                nombre=nombre,
                                numero_telefono=numero_telefono)


########################################################################
if __name__ == "__main__":
    app.run(debug=True, port=5000)